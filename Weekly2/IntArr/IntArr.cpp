/**********************************************
* File: IntArr.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is a solution to the Integer Array question 
* Compilation command: g++ -g -std=c++11 -Wpedantic IntArr.cpp -o IntArr
 **********************************************/
#include <iostream>
#include <unordered_map>
#include <list>

/********************************************
* Function Name  : recurSum
* Pre-conditions :size_t i
* Post-conditions: size_t
*
* Returns the summataion of all numbers from 0 
* to i. Assumes input is >= 0  
********************************************/
size_t recurSum(size_t i){

	if(i == 0)
		return 0;

	return i + recurSum(i - 1);
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function for the program  
********************************************/
int main(int argc, char** argv){

	int pairArray[19] = {4, 8, 0, 1, 7, 6, 2, 6, 10, 3, 6, 6, 7, 1, 0, 12, 6, 11, 12}; 

	// Print the array to the user
	std::cout << "The input array is: ";
	for(int i = 0; i < 18; i++){
		std::cout << pairArray[i] << " ";
	}
	std::cout << std::endl;

	// Create a HashMap using a list of integers as the values
	// The list of values represents the location in the array
	std::unordered_map< int, std::list<int> > HashPairs;

	// Iterate through the entire array
	for(int i = 0; i < 18; i++){

		// If it is not in the array, insert the key and the current element
		if(HashPairs.count(pairArray[i]) == 0){
			// Create a temporary list
			std::list<int> temp;

			// Insert the location into the temporary element
			temp.push_back(i);

			// Insert the new element into the Hash Table
			HashPairs.insert({pairArray[i], temp}); 
		}
		else{
			// If the element is in the array, push onto the list
			HashPairs[pairArray[i]].push_back(i);
		}
	}

	// Hash each value through the first half and see if the pair exists
	for(int i = 0; i < 12 / 2; i++){
		
		// Since there is a pair, we can assume that, if there is not a match, a pair does not exist
		if(HashPairs.count(i) != 0 && HashPairs.count(12-i) != 0){

			std::cout << "Pairs for (" << i <<  ", " << 12-i << "): ";

			// Iterate through all the elements in i and then all in 12-i
			for(std::list<int>::iterator iter = HashPairs[i].begin(); 
					iter != HashPairs[i].end(); iter++){
				
				for(std::list<int>::iterator jter = HashPairs[12 - i].begin();
					jter != HashPairs[12 - i].end(); jter++){
					std::cout << "[" << *iter << " " << *jter << "] ";
				}
			}
		}
		else{
			std::cout << "No pair for " << i << " and " << 12-i;
		}
		std::cout << std::endl;
	}

	// For the case where the input is an even number, and we need to check for the middle number
	if( (12 / 2) % 2 == 0 ){

		if(HashPairs.count( 12/2 ) != 0){
			if(HashPairs[12 / 2].size() >= 2){
				std::cout << "There are " << HashPairs[12 / 2].size() << " entries for " << 12/2 << ":";

				for(std::list<int>::iterator iter = HashPairs[12 / 2].begin();
						iter != HashPairs[12 / 2].end(); iter++){
				
					std::cout << " [" << *iter << "]";
				}

				std::cout << ". Therefore, there are " << recurSum(HashPairs[12 / 2].size() - 1);
			       	std::cout << " unique pairs of " << 12/2;	
			}
			std::cout << std::endl;
		}
	}

	return 0;

}

