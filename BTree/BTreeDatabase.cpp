/**********************************************
* File: BTreeInClass.cpp
* Author: [MUST PUT YOUR NAME HERE]
* Email: [MUST PUT YOUR E-MAIL HERE]
*
* This is the driver function for your in-class
* assignment
*
* Must push the following files to GitLab
* BTreeInClass.cpp
* BTree.h - modified with your
* BTreeNode.h - modified with your
**********************************************/

#include "BTree.h"
#include <iostream>
#include <string>
#include <time.h>
struct Data{
	int key;
	int data;

	Data(int Key, int Data): key(Key), data(Data){}
	bool operator<(const Data& rhs) const{
		if(key < rhs.key)
			return true;
		else if(key == rhs.key){
			if(data < rhs.data)
				return true;
		}
		return false;
	}
	bool operator>(const Data& rhs) const{
		if(key > rhs.key)
			return true;
		else if(key == rhs.key){
			if(data > rhs.data)
				return true;
		}
		return false;
	}
	bool operator==(const Data& rhs) const{
		if(key != rhs.key)
			return false;
		else{
			if(data != rhs.data)
				return false;
		}
		return true;
	}
	friend std::ostream& operator<<(std::ostream& outStream, const Data& block);
};
std::ostream& operator<<(std::ostream& outStream, const Data& block){
	outStream << block.key << ", " << block.data;
	return outStream;
}
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* This is the main driver function for the program
********************************************/
int main(int argc, char** argv){
	
	// Initial test code
	BTree<Data> dataBlocks(2);
	std::srand(time(NULL));
	Data newBlock (std::rand()%1000, std::rand()%5000);
	std::cout << newBlock << std::endl;
	//dataBlocks.insert(newBlock);
	for(int i = 0; i < 1000; i++){
	//	Data newBlock (rand()%2000, rand()%5000);
	//	dataBlocks.insert(newBlock);
	}
	
	// Initial print out!
    std::cout << "Traversal of tree constructed is\n"; 
    dataBlocks.traverse(); 
    std::cout << std::endl; 
	
	//std::cout << "Finding 'structure'" << std::endl;
	// The function calling your code here
	// Uncomment the next line, which will be your call 
	//inClass.printFoundNodes("struggle");

	return 0;
}
