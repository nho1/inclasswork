#include <iostream> 
#include <list> 
#include <limits.h> 
  
using namespace std; 
//Saves the vertices to delete edge
int init, fin;
/*****************
 *Graph Class
 *Inputs: None
 *outputs: None
 *This just creates an adjacency list with the functions needed
 * ****************/
class Graph 
{ 
	int V;    // No. of vertices 
	list<int> *adj;   
	bool isCyclicUtil(int v, bool visited[], bool *rs);   
	public: 
	Graph(int V);   // Constructor 
	void addEdge(int v, int w);   // to add an edge to graph 
	void removeEdge(int v, int w); // to delete an edge to graph
	bool isCyclic();    // returns true if there is a cycle in this graph 
							
}; 
  
/*****************
 *Graph Consstructor
 *Inputs: int v
 *outputs: None
 *This is the constructor for the Graph, and creates V lists
 * ****************/
Graph::Graph(int V) 
{ 
	this->V = V; 
	adj = new list<int>[V]; 
			
} 
  
/*****************
 *addEdge
 *Inputs: int v, int w
 *outputs: None
 *This adds an edge to the graph
 * ****************/
void Graph::addEdge(int v, int w) 
{ 
	    adj[v].push_back(w); // Add w to v’s list. 
		
} 

/*****************
 *removeEdge
 *Inputs: int v, int w
 *outputs: None
 *This deletes an edge to the graph
 * ****************/
void Graph::removeEdge(int v, int w){
	list<int>::iterator it = adj[v].begin();
	for(int i = 0; i < adj[v].size(); i++){
		if( *it == w ){
			adj[v].erase(it);
			break;
		}
		advance(it, 1);
	}
}


/*****************
 *isCyclicUtil
 *Inputs: int v, bool visited[], bool *recStack
 *outputs: bool
 *Detects whether there is a cycle in one of the adjacency lists
 * * ****************/
bool Graph::isCyclicUtil(int v, bool visited[], bool *recStack) { 
	 if(visited[v] == false) { 
     // Mark the current node as visited and part of recursion stack 
	     visited[v] = true;
		 cout << v << " "; 
    	 recStack[v] = true;           
	 // Recur for all the vertices adjacent to this vertex 
     	list<int>::iterator i; 
     	for(i = adj[v].begin(); i != adj[v].end(); ++i) { 
     		if ( !visited[*i] && isCyclicUtil(*i, visited, recStack)){ 
				return true; 
			}else if (recStack[*i]){    		
				init = recStack[*i];
				fin = v;
				cout << "Vertices " << init << " and " << fin << " make a cycle." << endl;
				return true;		   	
     		}                                                                        
     	}
	 } 
    recStack[v] = false;  // remove the vertex from recursion stack 
    return false; 
} 
                                                                                                                                
/*****************
 *iscyclic
 *Inputs: none
 *outputs: bool
 *Checks to see if the entire graph has a cycle; calls isCyclic Util
 * ****************/
// Returns true if the graph contains a cycle, else false.  
bool Graph::isCyclic() 
 { 
   // Mark all the vertices as not visited and not part of recursion stack 
       bool *visited = new bool[V]; 
       bool *recStack = new bool[V]; 
       for(int i = 0; i < V; i++) { 
       visited[i] = false; 
       recStack[i] = false; 
       } 
                                                                                                                 
 // Call the recursive helper function to detect cycle in different DFS trees 
       for(int i = 0; i < V; i++) 
         if (isCyclicUtil(i, visited, recStack)) 
               return true; 
         return false; 
  }

/*****************
 *main
 *Inputs: none
 *outputs: int
 *This is the main driver function that creates a graph and checks for cycle
 * ****************/
int main() 
{ 
// Create the graph 
   Graph g(4); 
   g.addEdge(0, 1); 
   g.addEdge(0, 2); 
   g.addEdge(1, 2); 
   g.addEdge(2, 0); 
   g.addEdge(2, 3); 
   g.addEdge(3, 3); 
   
   if(g.isCyclic()){
		g.removeEdge(init, fin);
		cout << endl << "Edge between " << init << " and " << fin << " was removed." << endl;
	}
   cout << "All edges removed" << endl;
   return 0;                                                           
} 
