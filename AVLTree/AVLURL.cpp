/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct Website{
	std::string url;
	std::string webName;

	Website(std::string url, std::string webName): url(url), webName(webName){}
	bool operator<(const Website& rhs) const{
		if(url < rhs.url)
			return true;
		else if(url == rhs.url){
			if(webName < rhs.webName)
				return true;
		}
		return false;
	}
	bool operator>(const Website& rhs) const{
		if(url > rhs.url)
			return true;
		else if(url == rhs.url){
			if(webName > rhs.webName)
				return true;
		}
		return false;
	}
	bool operator==(const Website& rhs) const{
		if(url != rhs.url)
			return false;
		else{
			if(webName != rhs.webName)
				return false;
		}
		return true;
	}
	friend std::ostream& operator<<(std::ostream& outStream, const Website& printWeb);
};
std::ostream& operator<<(std::ostream& outStream, const Website& printWeb){
	outStream << printWeb.url << ", " << printWeb.webName;
	return outStream;
}
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    AVLTree<Website> avlWebsite;
	Website Aardvark("google.com","Google");
	Website BadMean("nd.edu","Notre Dame Website");
	Website Aardvark2("amazon.com","Amazon");
	Website Aardvark4("wikipedia.com","Wikipedia");
	Website Aardvark5("reddit.com","Reddit");
	Website Website6("blizzard.com","Blizzard");
	Website Website7("leagueoflegends.com","League of Legends");
	Website Website8("youtube.com","Youtube");
	Website Website9("example.com","Example");
	Website Website10("example.com","Example");
	Website Website11("fbi.gov","FBI");
	avlWebsite.insert(Aardvark2);
	avlWebsite.insert(Aardvark);
	avlWebsite.insert(BadMean);
	avlWebsite.insert(Aardvark4);
	avlWebsite.insert(Aardvark5);
	avlWebsite.insert(Website6);
	avlWebsite.insert(Website7);
	avlWebsite.insert(Website8);
	avlWebsite.insert(Website9);
	avlWebsite.insert(Website10);
	avlWebsite.insert(Website11);
	avlWebsite.printTree();
	std::cout << "----" << std::endl;
	avlWebsite.remove(Aardvark);
	avlWebsite.printTree();

    return 0;
}
