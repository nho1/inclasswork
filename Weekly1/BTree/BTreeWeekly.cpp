/**********************************************
* File: BTreeWeekly.cpp
* Author: Matthew Morrison
* Email: matt.morrison@gmail.com
*
* This is the driver function for the solution to 
* the Weekly Coding Assignment for the B-Tree 
*
* Compilation Instructions:
* g++ -g -std=c++11 -Wpedantic BTreeWeekly.cpp -o BTreeWeekly 
* ./BTreeWeekly
*
* BTreeWeekly.out shows a successful run
**********************************************/

#include "BTree.h"
#include <iostream>

int totalRandom = 1000;
int checkRandom = 10;

struct Data{
	int key;	// Unique key 
	int data;
	
	/********************************************
	* Function Name  : data
	* Pre-conditions : 0
	* Post-conditions: Data() : key(0),
	* 
	* Empty Constructor 
	********************************************/
	Data() : key(0), data(0){ }
	
	/********************************************
	* Function Name  : data
	* Pre-conditions : d
	* Post-conditions: Data(int k, int d) : key(k),
	* 
	* Data constructor 
	********************************************/
	Data(int k, int d) : key(k), data(d) {}


	/********************************************
	* Function Name  : data
	* Pre-conditions : copy.data
	* Post-conditions: Data(const Data& copy) : key(copy.key),
	* 
	* Copy constructor 
	********************************************/
	Data(const Data& copy) : key(copy.key), data(copy.data){}

	/********************************************
	* Function Name  : ~Data
	* Pre-conditions : none
	* Post-conditions: none
	* 
	* Data destructor 
	********************************************/
	~Data() {}

	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Data& rhs
	* Post-conditions: bool
	* 
	* Overloaded == operator 
	********************************************/
	bool operator==(const Data& rhs) const{

		return key == rhs.key;
	}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Data& rhs
	* Post-conditions: bool
	* 
	* Overloaded < operator 
	********************************************/
	bool operator<(const Data& rhs) const{

		return key < rhs.key;
	}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Data& rhs
	* Post-conditions: bool
	* 
	* Overloaded < operator 
	********************************************/
	bool operator>(const Data& rhs) const{

		return key > rhs.key;
	}
	
	/********************************************
	* Function Name  : operator=
	* Pre-conditions : const Data& rhs
	* Post-conditions: Data&
	* 
	* Overloaded = operator 
	********************************************/
	Data& operator=(const Data& rhs){
		key = rhs.key;
		data = rhs.data;
		
		return *this;
	}
	
	/********************************************
	* Function Name  : operator<<
	* Pre-conditions : std::ostream& outStream, const Data& printData
	* Post-conditions: friend std::ostream&
	* 
	* Friend overloaded<< operator 
	********************************************/
	friend std::ostream& operator<<(std::ostream& outStream, const Data& printData);
};

std::ostream& operator<<(std::ostream& outStream, const Data& printData){
	
	outStream << "(" << printData.key << ", " << printData.data << ") ";
	
	return outStream;
}

/********************************************
* Function Name  : initDataTest
* Pre-conditions : none
* Post-conditions: none
* 
* Initial Data Constructor Test 
********************************************/
void initDataTest(){

	Data emptyData;

	Data data1(10, 3);
	
	Data data2(5, 3);

	Data data3(data1);
	
	Data data4(15, 3);
	
	std::cout << data1 << data2 << data3 << data4 << std::endl;
	
	std::cout << "Test 1 == 3: " << (data1 == data3) << std::endl;
	std::cout << "Test 2 == 3: " << (data2 == data3) << std::endl;
	std::cout << "Test 4 > 1: " << (data4 > data1) << std::endl;
	std::cout << "Test 1 < 4: " << (data1 < data4) << std::endl;
	
	Data data5 = data4;
	
	//std::cout << data5.key << " " << data5.data << std::endl;
	std::cout << data5 << std::endl;

}

/********************************************
* Function Name  : testBTree
* Pre-conditions : BTree<Data>* testBT
* Post-conditions: none
* 
* testing B-Tree insert 
********************************************/
void testBTree(BTree<Data>* testBT){
	
	std::cout << "Testing. Inserting " << checkRandom << " elements into the B-Tree" << std::endl;
	
	for(int iter = 0; iter < checkRandom; iter++){
		
		Data elem(iter, rand()%totalRandom);
		
		testBT->insert(elem);
		
	}
	
	testBT->traverse();
	std::cout << std::endl;
	
	testBT->printFoundNodes(Data(9,0));
	
	
}

/********************************************
* Function Name  : getRandElements
* Pre-conditions : BTree<Data>* database
* Post-conditions: none
* 
* Insert totalRandom Data Elements into the B-Tree 
********************************************/
void getRandElements(BTree<Data>* database){
	
	std::cout << "Inserting " << totalRandom << " elements into the B-Tree" << std::endl;
	
	for(int iter = 0; iter < totalRandom; iter++){
		
		Data elem(rand()%totalRandom, rand()%totalRandom);
		
		database->insert(elem);
		
	}
}

/********************************************
* Function Name  : checkBTreeNode
* Pre-conditions : BTreeNode<Data>* foundNode
* Post-conditions: none
* 
* Gets a valid BTreeNode and prints the elements 
* in that node to the user 
********************************************/
void checkBTreeNode(BTreeNode<Data>* foundNode){
			
	for(int jter = 0; jter < foundNode->numKeys; jter++){
				
		std::cout << foundNode->keys[jter] << " ";
				
	}
	std::cout << std::endl;
}


/********************************************
* Function Name  : findRandom
* Pre-conditions : BTree<Data>* database
* Post-conditions: none
* 
* Finds 10 randomly generated elements and prints to the user
* whether they were found 
********************************************/
void findRandom(BTree<Data>* database){
	
	for(int iter = 0; iter < checkRandom; iter++){
		
		std::cout << "---------------------------------" << std::endl;
		
		Data val(rand()%totalRandom, 0);
		
		std::cout << "Testing. Finding Random element: " << val.key << std::endl;
		
		if(database->search(val) == nullptr){
			std::cout << "No element with key " << val.key << " in the database " << std::endl;
		}
		else{
			std::cout << "Element with key " << val.key << " was found in the database!" << std::endl;
			
			// Print entire path using printFoundNodes 
			// database->printFoundNodes(val);
			
			// Print using the pointer 
			checkBTreeNode(database->search(val));
		
		}
		
	}
	
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* This is the main driver function for the program
********************************************/
int main(int argc, char** argv){
	
  	//initDataTest();
	
	// Initialize a B-Tree Database
	BTree<Data> database(2);
	
	// Initialize the random number generator 
	srand(time(NULL));
	
	// Test the B-Tree 
	//testBTree(&database);
	
	// Insert Random Elements
	getRandElements(&database);
	
	// Find Random Elements 
	findRandom(&database);

	return 0;
}
