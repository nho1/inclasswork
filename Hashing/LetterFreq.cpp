#include <iterator>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <cctype>
/*****************
 *Function Name: getNextToken
 *input: istream in
 *output: string ans
 *The function takes an input stream, cuts out trailing whitespace, and sets all chars to lower
 *****************/
std::string getNextToken(std::istream &in)
{
	char c;
	std::string ans="";
	c=in.get();
	//Read in each character until you either hit a non-alphabetical character or reach EOF
	while(!std::isalpha(c) && !in.eof()){
		c=in.get();
	}
	//Convert each character to lowercase, and then push that character to back of string ans
	while(std::isalpha(c)){
		ans.push_back(std::tolower(c));
		c=in.get();
	}
	//Return ans
	return ans;
							
}
/***************
 *Function Name: main
 *input: void
 *output: int
 *The main driver function. Inputs words into map and then outputs results using for loop
 ***************/
int main()
{
	    std::map<std::string,int> words;
		std::ifstream fin("input.txt");

		std::string s;
		std::string empty ="";
		//Get each string from ifstream, pass it to function, and add 1 to the value of that key until EOF
		while((s=getNextToken(fin))!=empty )
		++words[s];
		//Print out the word, followed by the value stored by that word (acts as a key)
		for(std::map<std::string,int>::iterator iter = words.begin(); iter!=words.end(); ++iter)
			std::cout<<iter->first<<' '<<iter->second<<std::endl;
		return 0;
							
}
