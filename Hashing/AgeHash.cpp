<<<<<<< HEAD
#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>
/************
 *Function Name: main
 *Pre-conditions: int argc, char **argv
 *Post-conditions: int
 *Main Driver function. Solution
 ***********/
int main(int argc, char **argv){
	//Initial setup of hash table
	std::map<std::string, int> ageHashOrdered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36},{"James", 38}};
	//Iterator to got through AVL Tree
	std::map<std::string, int>::iterator iterOr;
	
	std::cout << "The Ordered ages are: " << std::endl;
	//Loop through Age HashOrdered
	for(iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++){
		//Prints out the key followed by the value
		std::cout << iterOr->first << " "<<iterOr->second << std::endl;
	}
	std::unordered_map<std::string, int> ageHashUnordered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36},{"James", 38}};
	std::unordered_map<std::string, int>::iterator iterUn;

	std::cout << "The Unodered ages are: " << std::endl;
	for(iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end();iterUn++){
		std::cout << iterUn->first << " " <<iterUn->second << std::endl;
	}
	//Find the value accordint to key (name, in this case)
	//For ordered, it orders the keys alphabetically
	//For unordered, it does have O(1) search capabilities
	//As long as you access the key correctly, you don't have to worry about order
	std::cout << "The Ordered Example: "<< ageHashOrdered["Matthew"]<<std::endl;
	std::cout << "The Unordered Example: "<<ageHashUnordered["Alfred"]<<std::endl;
	return 0;
}
=======
/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
// Libraries Here

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){

	
	return 0;
}
>>>>>>> upstream/master
